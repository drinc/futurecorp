<?php

namespace App\Http\Controllers;

use App\PaymentService\AnzPaymentService;
use App\PaymentService\NabPaymentService;
use App\PaymentService\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    //
    public function index(){

    }

    public function paymentProcess(Request $request)
    {
        // Retrieve the validated input data...
        $validator = Validator::make($request->all(), [
            'cardnumber' => 'required|max:22',
            'name' => 'required',
            'validuntil' => 'required',
            'amount' => 'required',
        ]);
 
        if ($validator->fails()) {
            return redirect('/payment')
                        ->withErrors($validator)
                        ->withInput();
        }

        $transaction = new Transaction();
        $transaction->provider = $request->type;
        $transaction->cardnumber = $request->cardnumber;
        $transaction->name = $request->name;
        $transaction->valid = $request->validuntil;
        $transaction->amount = $request->amount;

        
        $gateway = null;
        switch($request->type){
            case "anz":
                $gateway = new AnzPaymentService;
                break;
            case "nab":
                $gateway = new NabPaymentService;
                break;
            default:
                $gateway = null;
                break;
        }
        
        $response = $gateway->makePayment($transaction);
        
        //dump($response);
        redirect('/payment');
    }

}
