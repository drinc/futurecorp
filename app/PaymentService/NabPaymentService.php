<?php

namespace App\PaymentService;

use App\PaymentService\Transaction;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class NabPaymentService implements PaymentService
{
  
    public function makePayment(Transaction $transaction): Response
    {
        try {
            $xml = "<?xml version='1.0' encoding='utf-8'?>
                <from>
                    <card_number>".$transaction->cardnumber."</card_number>
                    <card_name>".$transaction->name."</card_name>
                    <cvv>000</cvv>
                </from>
                <amount>".$transaction->amount."</amount>
                <merchant_id>".env('MERCHENT_ID')."</merchant_id>
                <merchant_key>".env('MERCHENT_KEY')."</merchant_key>";

            $res = Http::withHeaders(["Content-Type" => "text/xml;charset=utf-8"])
                ->post('https://nab.com/api/payment', ['body' => $xml]);
        }catch(Exception $e){
            new Response($e);
        }
        $resXml = "<?xml version='1.0' encoding='UTF-8'?>
                <from>
                  <card_number>123</card_number>
                </from>
                <amount>1.00</amount>
                <transaction_number>abc1234</transaction_number>
                <transaction_time>1914-06-28 14:06:28</transaction_time>";
        return new Response($resXml);
    }

}