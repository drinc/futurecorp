<?php

namespace App\PaymentService;

use App\PaymentService\Transaction;
use Exception;
use Illuminate\Http\Response;
use App\PaymentService\HttpClient;

class AnzPaymentService implements PaymentService
{
  
    public function makePayment(Transaction $transaction): Response
    {

        $client = new HttpClient();
        try {
            $res = $client->send([ 'form' => [
                                'card_number' => $transaction->cardnumber,
                                'card_name' => $transaction->name,
                                'cvv' => '123',
                            ],
                            'amount' => $transaction->amount,
                            'merchant_id' => env('MERCHENT_ID'),
                            'merchant_key' => env('MERCHENT_KEY')
                        ]);
        }catch(Exception $e)
        {
            //return new Response($e->getMessage(), 500);

            $res = '{
                        "from":[
                            {
                                "card_number":"'.$transaction->cardnumber.'"
                            }
                        ],
                        "amount":'.$transaction->amount.',
                        "transaction_number":"abc12345",
                        "transaction_time": "1914-06-28 14:06:28"
                    }';

            return new Response($res, 200);
        }       
     
        return $res;
    }
  
}