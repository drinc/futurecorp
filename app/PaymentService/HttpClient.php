<?php
namespace App\PaymentService;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

class HttpClient
{
    public function send(array $payload) : Response
    {
        return app(Client::class)
            ->request('POST', 'https://anz.com/api/payment', $payload);
    }
}