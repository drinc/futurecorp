<?php

namespace App\PaymentService;

use App\PaymentService\Transaction;
use Illuminate\Http\Response;

interface PaymentService
{
    public function makePayment(Transaction $transaction) : Response;
}