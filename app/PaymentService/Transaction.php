<?php

namespace App\PaymentService;


class Transaction {

    public string $provider = 'anz';
    public string $cardnumber = '';
    public string $name = '';
    public string $valid = '';
    public float $amount = 0.00;
    
}