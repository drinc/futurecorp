<?php

namespace Tests\Feature;

use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\File;
use Tests\TestCase;

class PaymentTest extends TestCase
{

    use WithFaker;


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Api client mock test
     */
    public function testAnzApi()
    {
        $mock = $this->mock(HttpClient::class);
        $mock->shouldReceive('send')
             ->andReturn(new Response(
                $status = 200,
                $headers = [],
                File::get(base_path('tests/data/api-response.json'))
            ));
    }

    /**
     * ANZ test
     *
     * @return void
     */
    public function test_anz()
    {
        $req = [];
        $req['type'] = "anz";
        $req['cardnumber'] = $this->faker->creditCardNumber();
        $req['name'] = $this->faker->name();
        $req['validuntil'] = $this->faker->month().'/2023';
        $req['amount'] = $this->faker->randomNumber(3);

        $response = $this->post('/payment', $req);

        $response->assertStatus(200);
    }

    /**
     * NAB test
     *
     * @return void
     */
    public function test_nab()
    {
        $req = [];
        $req['type'] = "nab";
        $req['cardnumber'] = $this->faker->creditCardNumber();
        $req['name'] = $this->faker->name();
        $req['validuntil'] = $this->faker->month().'/2023';
        $req['amount'] = $this->faker->randomNumber(3);

        $response = $this->post('/payment', $req);

        $response->assertStatus(200);
    }

    /**
     * NAB test 2
     *
     * @return void
     */
    public function test_nab_wrong_credit_card()
    {
        $req = [];
        $req['type'] = "nab";
        $req['cardnumber'] = '1234';
        $req['name'] = $this->faker->name();
        $req['validuntil'] = $this->faker->month().'/2023';
        $req['amount'] = $this->faker->randomNumber(3);

        $response = $this->post('/payment', $req);

        $response->assertStatus(200);
    }

    /**
     * wrong gateway
     *
     * @return void
     */
    public function test_wrong_gateway()
    {
        $req = [];
        $req['type'] = "nba";
        $req['cardnumber'] = '1234';
        $req['name'] = $this->faker->name();
        $req['validuntil'] = $this->faker->month().'/2023';
        $req['amount'] = $this->faker->randomNumber(3);

        $response = $this->post('/payment', $req);

        $response->assertStatus(500);
    }
    
}
